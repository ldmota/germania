CREATE TABLE `oc_lojas` (
 `loja_id` int(11) NOT NULL AUTO_INCREMENT,
 `nome` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
 `cidade` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
 `estado` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
 `endereco` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
 `telefone` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
 `facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
 PRIMARY KEY (`loja_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci