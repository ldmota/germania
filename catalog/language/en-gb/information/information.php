<?php

$_['heading_title']  = 'Solicitação de franquia';

// Text
$_['text_error'] = 'Information Page Not Found!';
$_['text_franqueado'] = 'Cliente interessado em se tornar um franqueado.';

// Text
$_['text_success']   = '<p>Seu pedido para se tornar um franqueado foi enviado com sucesso!</p>';

// Email
$_['email_subject']  = 'Pedido de Franquia';

