<?php

$_['heading_title']  = 'Solicitação de revenda';

// Text
$_['text_error'] = 'Information Page Not Found!';
$_['text_franqueado'] = 'Cliente interessado em se tornar um revendedor.';

// Text
$_['text_success']   = '<p>Seu pedido para se tornar um revendedor foi enviado com sucesso!</p>';

// Email
$_['email_subject']  = 'Pedido de Revenda';

