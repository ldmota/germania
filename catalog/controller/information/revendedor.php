<?php
class ControllerInformationRevendedor extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('information/revendedor');

		$this->document->setTitle($this->language->get('heading_title'));

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$message  = $this->language->get('text_revendedor') . "\n\n";
			$message .= 'Nome: ' . $this->request->post['name'] . "\n";
			$message .= 'E-mail: ' . $this->request->post['email'] . "\n";
			$message .= 'Data de nascimento: ' . $this->request->post['nascimento'] . "\n";
			$message .= 'CEP: ' . $this->request->post['cep'] . "\n";
			$message .= 'Endereço: ' . $this->request->post['endereco'] . "\n";
			$message .= 'Número: ' . $this->request->post['numero'] . "\n";
			$message .= 'Complemento: ' . $this->request->post['complemento'] . "\n";
			$message .= 'Estado: ' . $this->request->post['estado'] . "\n";
			$message .= 'Cidade: ' . $this->request->post['cidade'] . "\n";
			$message .= 'Telefone: ' . $this->request->post['telefone'] . "\n";

			$mail = new Mail();
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
			$mail->smtp_username = $this->config->get('config_mail_smtp_username');
			$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
			$mail->smtp_port = $this->config->get('config_mail_smtp_port');
			$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

			$mail->setTo($this->config->get('config_email'));
			$mail->setFrom($this->request->post['email']);
			$mail->setSender(html_entity_decode($this->request->post['name'], ENT_QUOTES, 'UTF-8'));
			$mail->setSubject(html_entity_decode($this->language->get('email_subject'), ENT_QUOTES, 'UTF-8'));
			$mail->setText($message);
			$mail->send();

			$this->response->redirect($this->url->link('information/revendedor/success'));
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('information/revendedor')
		);

		$data['heading_title'] = $this->language->get('heading_title');

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = '';
		}

		if (isset($this->error['email'])) {
			$data['error_email'] = $this->error['email'];
		} else {
			$data['error_email'] = '';
		}

		if (isset($this->error['nascimento'])) {
			$data['error_nascimento'] = $this->error['nascimento'];
		} else {
			$data['error_nascimento'] = '';
		}

		if (isset($this->error['cep'])) {
			$data['error_cep'] = $this->error['cep'];
		} else {
			$data['error_cep'] = '';
		}

		if (isset($this->error['endereco'])) {
			$data['error_endereco'] = $this->error['endereco'];
		} else {
			$data['error_endereco'] = '';
		}

		if (isset($this->error['numero'])) {
			$data['error_numero'] = $this->error['numero'];
		} else {
			$data['error_numero'] = '';
		}

		if (isset($this->error['complemento'])) {
			$data['error_complemento'] = $this->error['complemento'];
		} else {
			$data['error_complemento'] = '';
		}

		if (isset($this->error['estado'])) {
			$data['error_estado'] = $this->error['estado'];
		} else {
			$data['error_estado'] = '';
		}

		if (isset($this->error['cidade'])) {
			$data['error_cidade'] = $this->error['cidade'];
		} else {
			$data['error_cidade'] = '';
		}

		if (isset($this->error['telefone'])) {
			$data['error_telefone'] = $this->error['telefone'];
		} else {
			$data['error_telefone'] = '';
		}

		if (isset($this->request->post['name'])) {
			$data['name'] = $this->request->post['name'];
		} else {
			$data['name'] = "";
		}

		if (isset($this->request->post['email'])) {
			$data['email'] = $this->request->post['email'];
		} else {
			$data['email'] = "";
		}

        if (isset($this->request->post['nascimento'])) {
            $data['nascimento'] = dateDb($this->request->post['nascimento']);
        } else {
            $data['nascimento'] = "";
        }

		if (isset($this->request->post['cep'])) {
			$data['cep'] = $this->request->post['cep'];
		} else {
			$data['cep'] = "";
		}

		if (isset($this->request->post['endereco'])) {
			$data['endereco'] = $this->request->post['endereco'];
		} else {
			$data['endereco'] = "";
		}

		if (isset($this->request->post['numero'])) {
			$data['numero'] = $this->request->post['numero'];
		} else {
			$data['numero'] = "";
		}

		if (isset($this->request->post['complemento'])) {
			$data['complemento'] = $this->request->post['complemento'];
		} else {
			$data['complemento'] = "";
		}

		if (isset($this->request->post['estado'])) {
			$data['estado'] = $this->request->post['estado'];
		} else {
			$data['estado'] = "";
		}

		if (isset($this->request->post['cidade'])) {
			$data['cidade'] = $this->request->post['cidade'];
		} else {
			$data['cidade'] = "";
		}

		if (isset($this->request->post['telefone'])) {
			$data['telefone'] = $this->request->post['telefone'];
		} else {
			$data['telefone'] = "";
		}
		
		$data['action'] = $this->url->link('information/revendedor', '', true);

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('information/revendedor', $data));
	}

	protected function validate() {
		if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 32)) {
			$this->error['name'] = $this->language->get('error_name');
		}

		if (!filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)) {
			$this->error['email'] = $this->language->get('error_email');
		}

		if (utf8_strlen($this->request->post['telefone']) < 8) {
			$this->error['telefone'] = $this->language->get('error_telefone');
		}

		if (utf8_strlen($this->request->post['cep']) < 9) {
			$this->error['cep'] = $this->language->get('error_cep');
		}

        $d = date_parse(dateDb($this->request->post['nascimento']));
        if ( !checkdate($d['month'], $d['day'], $d['year']) ) {
            $this->error['nascimento'] = $this->language->get('error_nascimento');
        }

        if (utf8_strlen($this->request->post['numero']) < 1) {
            $this->error['numero'] = $this->language->get('error_numero');
        }

        if (utf8_strlen($this->request->post['endereco']) < 1) {
            $this->error['endereco'] = $this->language->get('error_endereco');
        }

        if (utf8_strlen($this->request->post['complemento']) < 1) {
            $this->error['complemento'] = $this->language->get('error_complemento');
        }

        if (utf8_strlen($this->request->post['estado']) < 1) {
            $this->error['estado'] = $this->language->get('error_estado');
        }

        if (utf8_strlen($this->request->post['cidade']) < 1) {
            $this->error['cidade'] = $this->language->get('error_cidade');
        }

		return !$this->error;
	}

	public function success() {
		$this->load->language('information/revendedor');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('information/revendedor')
		);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_message'] = $this->language->get('text_success');

		$data['button_continue'] = $this->language->get('button_continue');

		$data['continue'] = $this->url->link('common/home');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('common/success', $data));
	}
}