<?php
class ControllerInformationLojas extends Controller {
	public function index() {
		$this->load->language('catalog/information');

		$this->load->model('catalog/information');

		$data['lojas'] = array();

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$data['lojas'] = $this->model_catalog_information->getLojas($this->request->post);
		}

		$data['estados'] = $this->model_catalog_information->getEstados();

		$data['action'] = $this->url->link('information/lojas', '', true);

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('information/lojas')
		);

		$data['heading_title'] = $this->language->get('heading_title');
		
		if (isset($this->error['cidade'])) {
			$data['error_cidade'] = $this->error['cidade'];
		} else {
			$data['error_cidade'] = array();
		}
		
		if (isset($this->error['estado'])) {
			$data['error_estado'] = $this->error['estado'];
		} else {
			$data['error_estado'] = array();
		}

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('information/lojas', $data));
	}

	protected function validateForm() {
		if (!$this->request->post['cidade']) {
			$this->error['cidade'] = $this->language->get('error_cidade');
		}

		if (!$this->request->post['estado']) {
			$this->error['estado'] = $this->language->get('error_estado');
		}

		return !$this->error;
	}

	public function cidades() {
		$this->load->model('catalog/information');
		$cidades = $this->model_catalog_information->getCidades( $this->request->get["estado"] );

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($cidades));		
	}
}