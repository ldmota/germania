<?php echo $header; ?>
<figure class="container">
<div class="row">
	<div class="banner-faq text-center">
		<h4 class="tit">PERGUNTAS FREQUENTES</h4>
	</div>
	</div>
</figure>


<section class="container">
	<div class="row">
		<div class="panel-group" id="accordion">
		  <div class="panel panel-default">
		    <div class="panel-heading">
		      <h4 class="panel-title">
		        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
		       Quem é a Cervejaria Germânia? </a>
		      </h4>
		    </div>
		    <div id="collapse1" class="panel-collapse collapse">
		      <div class="panel-body">
		      	<p>A Cervejaria GERMÂNIA é sua companheira que sempre pode te ajudar. Ela é firmeza e está presente na hora certa para os seus momentos mais especiais e felizes. É por isso que quando você precisa de um chopp geladinho, você pode contar com a Germânia.</p>
<p>A GERMÂNIA está á 25 anos satisfazendo seus consumidores com diversos chopes e cervejas. A cervejaria já ganhou 3 prêmios nacionais e um internacional, com a cerveja Amazônia. Então não tem erro se você pedir cerveja. </p>
		      </div>
		    </div>
		  </div>
		  <div class="panel panel-default">
		    <div class="panel-heading">
		      <h4 class="panel-title">
		        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
		        Menores de 18 anos podem comprar na Cervejaria Germânia? </a>
		      </h4>
		    </div>
		    <div id="collapse2" class="panel-collapse collapse">
		      <div class="panel-body">Não, o e-commerce da Cervejaria Germânia é destinado para maiores 18 anos, por vender bebidas alcoólicas, de acordo com a Lei nº 14.592.</div>
		    </div>
		  </div>
		  <div class="panel panel-default">
		    <div class="panel-heading">
		      <h4 class="panel-title">
		        <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
		        Como funciona a Germânia?</a>
		      </h4>
		    </div>
		    <div id="collapse3" class="panel-collapse collapse">
		      <div class="panel-body">
		      	<p>O nosso serviço funciona da seguinte forma:</p>
		      	<p>
1)	Escolha o seu Chopp ou a cerveja <br>
2)	Agende a entrega<br>
3)	O nosso técnico leva e instala a Chopeira na sua casa<br>
4)	Você aproveita o seu evento com os seus amigos, sem a bebida estará quente...<br>
5)	Nós recolhemos a Chopeira na sua casa
</p>
<p>A entrega e a retirada serão feitas nos períodos selecionados por você na hora da compra, sendo entre 8h as 19h. Antes da entrega, a nossa loja entrará em contato com você para confirmar o pedido, garantindo uma melhor experiência.</p>
<p>Os pedidos devem ser feitos com 48h de antecedência para que a nossa loja se prepare para a entrega e consiga atender da melhor forma.</p>
<p>A gente se encarrega de escolher a melhor Chopeira para o seu evento de acordo com a ocasião, número de pessoas e o local do evento. Para estimar a quantidade de Chopp necessária para o evento</p>
<p>Deixa com a gente, de Chopp a gente entende!</p>

		      </div>
		    </div>
		  </div>
		  <div class="panel panel-default">
		    <div class="panel-heading">
		      <h4 class="panel-title">
		        <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">
		        Onde a Cervejaria Germânia atende?</a>
		      </h4>
		    </div>
		    <div id="collapse4" class="panel-collapse collapse">
		      <div class="panel-body">
		      	<p>Até agora estamos experimentando... A Germânia quer ser a melhor para você. Então, por enquanto atendemos apenas toda a cidade de São Paulo. Mas não se preocupe, em breve vamos disponibilizar para todos que precisam de um chopp gelado. Cadastre-se no newsletter da Germânia que te enviamos as últimas novidades.</p>

		      </div>
		    </div>
		  </div>
		  <div class="panel panel-default">
		    <div class="panel-heading">
		      <h4 class="panel-title">
		        <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">
		        Quais os horários de atendimento da Cervejaria Germânia ?</a>
		      </h4>
		    </div>
		    <div id="collapse5" class="panel-collapse collapse">
		      <div class="panel-body">
		      	<p>
		      		A Germânia utiliza seus parceiros e franquias locais para entregarem o que você precisa. A Germânia está aberta de domingo á domingo, das 8h ás 19h, caso o seu pedido não tenha sido efetuado no horário apresentado este será confirmado no dia seguinte.  
		      	</p>

		      </div>
		    </div>
		  </div>
		  <div class="panel panel-default">
		    <div class="panel-heading">
		      <h4 class="panel-title">
		        <a data-toggle="collapse" data-parent="#accordion" href="#collapse6">
		        Quanto tempo leva a entrega da Germânia?</a>
		      </h4>
		    </div>
		    <div id="collapse6" class="panel-collapse collapse">
		      <div class="panel-body">
		      	<p>
		      		A Germânia é a amiga dos melhores caras. Por isso quando você pede algo para a Germânia a coisa é rápida. Sabemos que chopp não é brincadeira. E quem precisa, precisa para agora e gelado. O pedido leva no máximo 48h para ser computado e entregue na sua residência ou festa, podendo estar antes na sua casa, só depende do horário e a data marcada por você.
		      	</p>

		      </div>
		    </div>
		  </div>
		  <div class="panel panel-default">
		    <div class="panel-heading">
		      <h4 class="panel-title">
		        <a data-toggle="collapse" data-parent="#accordion" href="#collapse7">
		        Quais as formas de pagamento? </a>
		      </h4>
		    </div>
		    <div id="collapse7" class="panel-collapse collapse">
		      <div class="panel-body">
		      	<p>O pagamento pode ser feito com cartão de débito, crédito, boleto* ou em dinheiro na própria lojas. </p>
<p>*O boleto tem um tempo de confirmação de pedido maior de até 3 dias uteis</p>

		      </div>
		    </div>
		  </div>
		  <div class="panel panel-default">
		    <div class="panel-heading">
		      <h4 class="panel-title">
		        <a data-toggle="collapse" data-parent="#accordion" href="#collapse8">
		        Posso cancelar o meu pedido?</a>
		      </h4>
		    </div>
		    <div id="collapse8" class="panel-collapse collapse">
		      <div class="panel-body">
		      	<p>
		      		Com a Germânia não existe tempo ruim. Precisou cancelar, pegue o telefone e fale com a gente no 0800-110-420. Resolveremos o problema. Fazemos tudo muito rápido, e se você pensar demais um de nossos parceiros já poderá estar à sua porta.
		      	</p>

		      </div>
		    </div>
		  </div>
		</div>
	</div>
</section>
	
<?php echo $footer; ?>