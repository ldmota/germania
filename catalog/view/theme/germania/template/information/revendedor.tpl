<?php echo $header; ?>


		<figure class="container">
    <div class="row">
      <div class="banner-revendedor">
        <div class="col-md-3">
          <h4 class="tit">SEJA UM
REVENDENDOR
GERMÂNIA</h4>
          <p>Onde tem Germânia, tem sempre
um bom Chopp. E é assim que
o seu estabelecimento fica cheio
de clientes satisfeitos. Conquiste
o coração e o paladar do seu freguês com a linha de chopps e cervejas especiais da Germânia.</p>
        </div>
      </div>
      </div>
    </figure>


    <section class="container">
      <div class="row revend">
        <div class="col-md-6 padding0">
          <form class="row form" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
            <div class="col-md-12">
              <div class="form-group">
                <?php $error_class = $error_name?'error':'' ?>
                <input type="text" name="name" value="<?php echo $name ?>" id="input-name" class="form-control <?php echo $error_class; ?>" placeholder="Nome Completo*" />
              </div>
            </div>
            <div class="col-md-7">
              <div class="form-group">
                <?php $error_class = $error_email?'error':'' ?>
                <input type="text" name="email" value="<?php echo $email ?>" id="input-email" class="form-control <?php echo $error_class; ?>" placeholder="E-mail*" />
              </div>
            </div>
            <div class="col-md-5">
            <div class="form-group">
              <?php $error_class = $error_nascimento?'error':'' ?>
              <input type="text" name="nascimento" value="<?php echo $nascimento ?>" id="input-nascimento" class="form-control <?php echo $error_class; ?>" placeholder="Data de Nascimento*" />
            </div>
            </div>
            <div class="col-md-4">
            <div class="form-group">
              <?php $error_class = $error_cep?'error':'' ?>
              <input type="text" name="cep" value="<?php echo $cep ?>" id="input-cep" class="form-control <?php echo $error_class; ?>" placeholder="CEP*" />
            </div>
            </div>
            <div class="col-md-8">
            <div class="form-group">
              <?php $error_class = $error_endereco?'error':'' ?>
                <input type="text" name="endereco" value="<?php echo $endereco ?>" id="input-endereco" class="form-control <?php echo $error_class; ?>" placeholder="Endereço*" />
            </div>
            </div>
            <div class="col-md-3">
            <div class="form-group">
              <?php $error_class = $error_numero?'error':'' ?>
                <input type="text" name="numero" value="<?php echo $numero ?>" id="input-numero" class="form-control <?php echo $error_class; ?>" placeholder="Número*" />
            </div>
            </div>
            <div class="col-md-5">
            <div class="form-group">
              <?php $error_class = $error_complemento?'error':'' ?>
                <input type="text" name="complemento" value="<?php echo $complemento ?>" id="input-complemento" class="form-control <?php echo $error_class; ?>" placeholder="Complemento*" />
            </div>
            </div>
            <div class="col-md-4">
            <div class="form-group">
              <?php $error_class = $error_estado?'error':'' ?>
                <input type="text" name="estado" value="<?php echo $estado ?>" id="input-estado" class="form-control <?php echo $error_class; ?>" placeholder="Estado*" />
            </div>
            </div>
            <div class="col-md-7">
            <div class="form-group">
              <?php $error_class = $error_cidade?'error':'' ?>
                <input type="text" name="cidade" value="<?php echo $cidade ?>" id="input-cidade" class="form-control <?php echo $error_class; ?>" placeholder="Cidade*" />
            </div>
            </div>
            <div class="col-md-5">
            <div class="form-group">
              <?php $error_class = $error_telefone?'error':'' ?>
                <input type="text" name="telefone" value="<?php echo $telefone ?>" id="input-telefone" class="form-control <?php echo $error_class; ?>" placeholder="Telefone*" />
            </div>
            </div>
            <div class="col-md-12">
            <div class="form-group">
              <input type="submit" value="ENVIAR SOLICITAÇÃO" class="form-control">
            </div>
            </div>
          </form>

          <img class="img-responsive" src="images/img-form.jpg" alt="">
        </div>
        <div class="col-md-6 padding0">
          <img class="img-responsive" src="images/img-rev.jpg" alt="">
        </div>
      </div>
    </section>

<br><br><br><br><br><br>
		
<script type="text/javascript">
  $(document).ready(function(){
    $('#input-nascimento').mask('00/00/0000');
    $('#input-cep').mask('00000-000');
    var options =  {onKeyPress: function(tel, e, field, options){
      var masks = ['(00) 0000-0000Z', '(00) 00000-0000'];
      mask = (tel.length>14) ? masks[1] : masks[0];
      $('#input-telefone').mask(mask, options);
      }, translation: {
        'Z': {
          pattern: /[0-9]/, optional: true
      }
    }};
    $('#input-telefone').mask('(00) 0000-0000', options);
  });

</script>
	
		
<?php echo $footer; ?>