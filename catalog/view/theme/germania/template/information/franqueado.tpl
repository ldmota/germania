<?php echo $header; ?>


		<figure class="container">
    <div class="row">
        <img class="img-responsive" src="images/banner-franquia.jpg" alt="">
      </div>
    </figure>

    
      <section class="container text-center">
        <div class="row">
        <div class="bg-cinza">
        <h5>PREENCHA O FORMULÁRIO E AGUARDE NOSSO CONTATO:</h5>
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
          <div class="form-franquia">
            <div class="col-md-6">
              <div class="form-group">
                <?php $error_class = $error_name?'error':'' ?>
                <input type="text" name="name" value="<?php echo $name ?>" id="input-name" class="form-control <?php echo $error_class; ?>" placeholder="Nome Completo*" />
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <?php $error_class = $error_email?'error':'' ?>
                <input type="text" name="email" value="<?php echo $email ?>" id="input-email" class="form-control <?php echo $error_class; ?>" placeholder="E-mail*" />
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <?php $error_class = $error_nascimento?'error':'' ?>
                <input type="text" name="nascimento" value="<?php echo $nascimento ?>" id="input-nascimento" class="form-control <?php echo $error_class; ?>" placeholder="Nascimento*" />
              </div>
            </div>
            
            <div class="col-md-2">
              <div class="form-group">
                <?php $error_class = $error_cep?'error':'' ?>
                <input type="text" name="cep" value="<?php echo $cep ?>" id="input-cep" class="form-control <?php echo $error_class; ?>" placeholder="CEP*" />
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <?php $error_class = $error_endereco?'error':'' ?>
                <input type="text" name="endereco" value="<?php echo $endereco ?>" id="input-endereco" class="form-control <?php echo $error_class; ?>" placeholder="Endereço*" />
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <?php $error_class = $error_numero?'error':'' ?>
                <input type="text" name="numero" value="<?php echo $numero ?>" id="input-numero" class="form-control <?php echo $error_class; ?>" placeholder="Nº*" />
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <?php $error_class = $error_complemento?'error':'' ?>
                <input type="text" name="complemento" value="<?php echo $complemento ?>" id="input-complemento" class="form-control <?php echo $error_class; ?>" placeholder="Complemento*" />
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <?php $error_class = $error_estado?'error':'' ?>
                <input type="text" name="estado" value="<?php echo $estado ?>" id="input-estado" class="form-control <?php echo $error_class; ?>" placeholder="Estado*" />
              </div>
            </div>

            <div class="col-md-4">
              <div class="form-group">
                <?php $error_class = $error_cidade?'error':'' ?>
                <input type="text" name="cidade" value="<?php echo $cidade ?>" id="input-cidade" class="form-control <?php echo $error_class; ?>" placeholder="Cidade*" />
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <?php $error_class = $error_telefone?'error':'' ?>
                <input type="text" name="telefone" value="<?php echo $telefone ?>" id="input-telefone" class="form-control <?php echo $error_class; ?>" placeholder="Telefone*" />
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <input type="submit" class="form-control" value="ENVIAR SOLICITAÇÃO">
              </div>
            </div>
          </div>
        </form>
        </div>
        </div>
      </section>


      <section class="container">
        <div class="row">
          <div class="box-azul text-center">
            <h3>Porque escolher a Germânia</h3>
            <p>Somos uma cervejaria dedicada à criação de produtos e serviços inovadores. Em mais de 25 anos de história, consagramos as nossas bebidas e revolucionamos a nossa distribuição com a criação primeira rede de chopp delivery dvo Brasil: a Rede Lig Chopp Germânia. Hoje, nossas lojas levam chopp e cerveja de qualidade, com variedade no tamanho das latas e dos barris, para as regiões Sul, Sudeste e Centro-Oeste do país.</p>
          </div>
        </div>
      </section>

      <section class="container">
        <div class="row">
          <div class="box-preciso text-center">
            <h3>Você tem o que é preciso?</h3>
            <p>Saiba o que procuramos em nossos franqueados: <br> <br>

            Gostar de beber: identidade com o negócio,<br>
            vontade de aprender me senso comercial.<br>
            Nunca deixar o copo ficar vazio: gostar de servir as pessoas,<br>
            ter comprometimento e garra para realizar os seus sonhos.<br>
            Saber brindar: ser empreendedor, ter capacidade financeira<br>
            e conhecer a região de atuação.
            </p>
          </div>
        </div>
      </section>


      <section class="container">
        <div class="row">
          <div class="box-local text-center">
            <h3>A escolha do local</h3>
            <p>Nossas lojas estão em lugares movimentados<br>
            e fáceis de achar. Saiba de tudo aqui:<br>
            Você precisa: definir o público alvo, avaliar a infra-estrutura<br>
            e a localização do ponto...<br>
            Mas, relaxa que: você vai receber toda a orientação<br>
            na hora de escolher o seu ponto!<br>
            É agora: estamos em processo de expansão<br>
            no Estado de São Paulo!
            </p>
          </div>
        </div>
      </section>

      <section class="container">
        <div class="row">
          <div class="box-investir text-center">
            <h3>Quanto você precisa investir?</h3>
            <p>A partir de R$ 250 mil, você já entra para a Família Germânia:<br>
            O que entra nessa conta: equipamentos, veículo, estoque e capital de giro.<br>
            Você não se preocupa com: taxa de royalties e franquia mensal.<br>
            Estimativa de retorno: de 24 a 36 meses.<br>
            Faturamento mensal médio: 25 mil por mês (10% a 15%, de acordo com a ABF).<br>
            O seu ponto de equilíbrio é: 5.000 litros por mês.

            </p>
          </div>
        </div>
      </section>


      <section class="container">
        <div class="row">
          <div class="box-ajudinha text-center">
            <h3>Vai uma ajudinha?</h3>
            <p>Quem faz parte da Família Germânia, nunca está sozinho. A nossa parceria vai ser assim: <br><br>

Durante a pré-inauguração da sua loja:<br>
- Avaliação e aprovação do ponto comercial.v<br>
- Entrega dos manuais do sistema de franquia (implantação e gestão operacional).<br>
- Fornecimento do projeto para layout interno da loja.<br>
- Indicação de melhores fornecedores para a montagem da loja.<br>
- Capacitação do franqueado e equipe com apoio técnico e operacional.<br>
- Acompanhamento e suporte de marketing em ações pontuais.<br>
- Coordenação do projeto de implantação da unidade.<br>
- Fornecimento de equipe de montagem dedicada (consultor de franquia).<br><br>

Suporte contínuo (depois da inauguração da sua loja):<br>
- Fornecimento integral do Serviço de Atendimento ao Franqueado (SAF),<br>
- Assessoria de marketing para ações regionais e desenvolvimento de PDV.<br>
- Consultoria de campo especializada e supervisão periódica.<br>
- Reuniões mensais com a franqueadora e anuais com toda a rede.<br>
- Capacitação para reciclagem do franqueado e equipe.<br>
- Serviço de Call Center e E-commerce para fomentar as vendas aos lojistas.<br>
- Suporte tecnológico e gestão de abastecimento.<br>
- Equipe corporativa de franquias e expansão dedicada.


            </p>
          </div>
        </div>
      </section>

<br><br><br><br><br><br>
		
<script type="text/javascript">
  $(document).ready(function(){
    $('#input-nascimento').mask('00/00/0000');
    $('#input-cep').mask('00000-000');
    var options =  {onKeyPress: function(tel, e, field, options){
      var masks = ['(00) 0000-0000Z', '(00) 00000-0000'];
      mask = (tel.length>14) ? masks[1] : masks[0];
      $('#input-telefone').mask(mask, options);
      }, translation: {
        'Z': {
          pattern: /[0-9]/, optional: true
      }
    }};
    $('#input-telefone').mask('(00) 0000-0000', options);
  });

</script>
	
		
<?php echo $footer; ?>