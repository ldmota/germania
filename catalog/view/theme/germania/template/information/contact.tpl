<?php echo $header; ?>
    <figure class="container">
    <div class="row">
      <div class="banner-orcamento text-center">
          <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
            <div class="col-md-5">
              <h5>Deixe seus dados e a <b>Germânia</b><br> entrará em contato com você!</h5>
              <div class="form-group">
                <input type="text" name="name" value="<?php echo $name; ?>" id="input-name" class="form-control" placeholder="Nome" />
                <?php if ($error_name) { ?>
                <div class="text-danger"><?php echo $error_name; ?></div>
                <?php } ?>                
              </div>
              <div class="form-group">
                <input type="text" name="telefone" value="<?php echo $telefone; ?>" id="input-telefone" class="form-control" placeholder="Telefone"/>
                <?php if ($error_telefone) { ?>
                <div class="text-danger"><?php echo $error_telefone; ?></div>
                <?php } ?>
              </div>
              <div class="form-group">
                <input type="text" name="email" value="<?php echo $email; ?>" id="input-email" class="form-control" placeholder="E-mail"/>
                <?php if ($error_email) { ?>
                <div class="text-danger"><?php echo $error_email; ?></div>
                <?php } ?>
              </div>
              <div class="form-group">
                <textarea placeholder="Mensagem" name="enquiry" rows="10" id="input-enquiry" class="form-control"><?php echo $enquiry; ?></textarea>
                <?php if ($error_enquiry) { ?>
                <div class="text-danger"><?php echo $error_enquiry; ?></div>
                <?php } ?>
              </div>
              <div class="form-group">
                <input type="submit" class="form-control" value="Enviar">
              </div>
            </div>
            <div class="col-md-7 hidden-xs">
              <img class="img-responsive" src="images/atendente.png" alt="">
            </div>
          </form>
      </div>
      </div>
    </figure>

<?php echo $footer; ?>
