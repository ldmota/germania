<?php echo $header; ?>

		<figure class="container">
			<div class="row">
			<div class="banner-capa text-center">
				
					<div class="col-md-3">
					<br>
						<img src="images/sempre-um-bom-chopp.png" alt="">
						<br>
					</div>
					<div class="col-md-3 hidden-xs">
						<p>Tudo começou em Vinhedo, cidade do interior de São Paulo, no ano de 1991. Com a escolha certa dos ingredientes e o cuidado dos mestres cervejeiros,
a Germânia conquistou o paladar dos brasileiros na hora de brindar os bons momentos da vida. </p>
					</div>
					<div class="col-md-3 hidden-xs">
						<p>Hoje, depois de mais de 25 anos,
a Germânia continua a ganhar mais e mais adeptos. É só olhar
a nossa história e ver que a receita certa para se fazer sempre um bom chopp é criar novos sabores, conquistar prêmios de qualidade
e novos países para divulgar
a cerveja brasileira.</p>
					</div>
					<div class="col-md-3">
						<a href="<?php echo $historia; ?>">						
							<img src="images/conheca.png" alt="">
						</a>
					</div>
					
				
			</div>
			</div>
		</figure>


		
<?php echo $footer; ?>