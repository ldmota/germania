<?php echo $header; ?>

		<figure class="container">
		<div class="row">
			<div class="banner-capa-loja text-center">				
				<div class="col-9">
					<div class="row">
						<form id="form-cidades" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
						<div class="col-md-6">
							<div class="form-group">
								<select name="estado" id="estado" class="form-control">
									<option value="">Selecione o estado</option>
									<?php foreach($estados as $estado) { ?>
										<option value="<?php echo $estado['estado']; ?>"><?php echo $estado['estado']; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<select name="cidade" id="cidade" class="form-control">
									<option value="">Cidade</option>
								</select>
							</div>
						</div>
						</form>
					</div>
					<?php foreach($lojas as $loja){ ?>
					<div class="row">
						<div class="col-md-12">
							<div class="resultado-loja">
								<h3><?php echo $loja['nome']; ?></h3>
								<p><?php echo $loja['endereco']; ?><p>
								<p><?php echo $loja['telefone'] ?> </p>
							</div>
						</div>
					</div>
					<?php } ?>

				</div>
			</div>
		</figure>

		<script>
	        $("#cidade").change(function(e){
	        	var cidade = $(this).val();
	        	$("#form-cidades").submit();
	        });

			$('select[name=\'estado\']').on('change', function() {
				$.ajax({
					url: 'index.php?route=information/lojas/cidades&estado=' + this.value,
					dataType: 'json',
					success: function(json) {
						html = '<option value="">Selecione uma cidade</option>';

						for (i = 0; i < json.length; i++) {
							html += '<option value="' + json[i]['cidade'] + '">';

							html += json[i]['cidade'] + '</option>';
						}

						$('select[name=\'cidade\']').html(html);
					},
					error: function(xhr, ajaxOptions, thrownError) {
						alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
					}
				});
			});        
      	</script>
		
<?php echo $footer; ?>