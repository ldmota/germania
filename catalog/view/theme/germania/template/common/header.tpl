<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $title; ?></title>
    <base href="<?php echo $base; ?>" />
    <?php if ($description) { ?>
    <meta name="description" content="<?php echo $description; ?>" />
    <?php } ?>
    <?php if ($keywords) { ?>
    <meta name="keywords" content= "<?php echo $keywords; ?>" />
    <?php } ?>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js"></script>
    <script src="catalog/view/javascript/jquery/owl-carousel/owl.carousel.min.js"></script>

    <script src="catalog/view/javascript/storyjs-embed.js"></script>
    <script src="catalog/view/javascript/common.js"></script>
    <script src="catalog/view/javascript/jquery/jquery.mask.js"></script>
    <!-- <script src="catalog/view/javascript/cidades-estados-utf8.js" type="text/javascript"></script> -->

    <script src="https://use.fontawesome.com/997f8d75b7.js"></script>

    <link rel="stylesheet" href="catalog/view/theme/default/stylesheet/stylesheet.css">
    <!-- Owl Stylesheets -->
    <link rel="stylesheet" href="catalog/view/theme/germania/stylesheet/css/owl.carousel.min.css">
    <link rel="stylesheet" href="catalog/view/theme/germania/stylesheet/css/owl.theme.default.min.css">
    <!-- Bootstrap -->
    <link href="catalog/view/theme/germania/stylesheet/css/bootstrap.min.css" rel="stylesheet">
    <link href="catalog/view/theme/germania/stylesheet/css/main.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php foreach ($styles as $style) { ?>
    <link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
    <?php } ?>
    <script src="catalog/view/javascript/common.js" type="text/javascript"></script>
    <?php foreach ($links as $link) { ?>
    <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
    <?php } ?>
    <?php foreach ($scripts as $script) { ?>
    <script src="<?php echo $script; ?>" type="text/javascript"></script>
    <?php } ?>
    <?php foreach ($analytics as $analytic) { ?>
    <?php echo $analytic; ?>
    <?php } ?>   
  </head>
  <body>
    <header>
      <div class="topo">
        <div class="container">
          <div class="row">
            <section class="menu-primary text-right">
              <ul class="social hidden-xs">
                <li><a href="https://www.facebook.com/germaniaoficial" target="_blank"><img src="catalog/view/theme/germania/stylesheet/images/facebook.png" alt=""></a></li>
                <li><a href="https://www.instagram.com/cervejariagermania/" target="_blank"><img src="catalog/view/theme/germania/stylesheet/images/instagram.png" alt=""></a></li>
              </ul>
              <ul class="link hidden-xs">
                <li><a href="index.php?route=information/franqueado">FRANQUEADO</a></li>
                <li><a href="index.php?route=information/revendedor">REVENDEDOR</a></li>
              </ul>
              <ul class="ico-lig hidden-xs">
                <li><a href="#"><img src="catalog/view/theme/germania/stylesheet/images/logo-lig.png" alt=""></a></li>
              </ul>
              <ul class="telefone">
                <li><img src="catalog/view/theme/germania/stylesheet/images/ico-tel.png" alt=""> 0800 110 420</li>
              </ul>
            </section>
            <div class="col-md-6">
              <?php if ($logo) { ?>
              <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /></a>
              <?php } else { ?>
              <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
              <?php } ?>

            </div>
            <div class="col-md-2 text-right">
              <div class="box-calcule" data-toggle="modal" data-target="#calculadora">
                <span>CALCULE SUA FESTA</span>
                <img src="catalog/view/theme/germania/stylesheet/images/calcule.png" alt="">
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">

              <div><?php echo $search; ?></div>

              <div><?php echo $cart; ?></div>

              </div>


        


            </div>
            <div class="col-md-1 text-right hidden-xs">
              <div class="dropdown">
                <?php if ($logged) { ?>
                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                  <img src="catalog/view/theme/germania/stylesheet/images/visitante.png" alt="">
                  <span class="caret"></span>
                  <p class="ola-visitante">Olá <br> <?php echo $name; ?></p>
                </button>
                 <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                  <li><a href="index.php?route=account/account">Minha Conta</a></li>
                  <li role="separator" class="divider"></li>
                  <li><a href="index.php?route=account/logout">Sair</a></li>
                </ul> 
                <?php } else { ?>

              <a class="btn btn-default" id="dropdownMenu1" style="color:#FFF;" href="<?php echo $login; ?>">
                  <img src="catalog/view/theme/germania/stylesheet/images/visitante.png" alt="">
                  
                  <p class="ola-visitante">Entrar</p>
                </a>
               

              <?php } ?>

              </div>
            </div>
          </div>
        </div>
      </div>
      <?php if ($categories) { ?>
        <nav class="navbar">
          <div class="container">
            <!-- <div class="navbar-header"><span id="category" class="visible-xs"><?php echo $text_category; ?></span> -->
              <button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"><i class="fa fa-bars"></i></button>
            </div>
            <div class="collapse navbar-collapse navbar-ex1-collapse">
              <ul class="nav nav-justified">
                <li><a href="<?php echo $base ?>">Início</a></li>
                <?php foreach ($categories as $category) { ?>
                <?php if ($category['children']) { ?>
                <!-- <li class="dropdown"><a href="<?php echo $category['href']; ?>" class="dropdown-toggle" data-toggle="dropdown"><?php echo $category['name']; ?></a>
                  <div class="dropdown-menu">
                    <div class="dropdown-inner">
                      <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
                      <ul class="list-unstyled">
                        <?php foreach ($children as $child) { ?>
                        <li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
                        <?php } ?>
                      </ul>
                      <?php } ?>
                    </div>
                    <a href="<?php echo $category['href']; ?>" class="see-all"><?php echo $text_all; ?> <?php echo $category['name']; ?></a> </div>
                </li> -->
                <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
                <?php } else { ?>
                <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
                <?php } ?>
                <?php } ?>
                
                <li><a href="index.php?route=information/capahistoria">Germania</a></li>
                <li><a href="index.php?route=information/ajuda">Dúvidas</a></li>
                <li><a href="<?php echo $contact; ?>">Orçamento</a></li>
              </ul>
            </div>
          </div>
        </nav>

      <?php } ?>

    </header>