    <div class="box-red">
      <section class="container newsl">
        <div class="row">
          <div class="col-md-3">
            <div class="ajusta-img"><img class="img-responsive hidden-xs" src="catalog/view/theme/germania/stylesheet/images/img-news.png" alt=""></div>
          </div>
          <div class="col-md-5">
            <div class="box-heading">Assine nossa Newsletter</div>
          </div>
          <div class="col-md-4 text-center">
            <div id="journal-newsletter-1812240441" class="journal-newsletter-10 box journal-newsletter text-left " style="; ">
              <div class="box-content" style="">
                <span class="newsletter-input-wrap" style="height: 40px">
                <input type="text" class="newsletter-email" placeholder="E-mail" style="">
                <a class="newsletter-button button" onclick="Journal.newsletter($('#journal-newsletter-1812240441'));" style="">Enviar</a>
                </span>
              </div>
            </div>
            <script>
              $("<style></style>").appendTo($('head'));
            </script>
          </div>
        </div>
      </section>
    </div>

    <footer>
      <div class="container">
        <div class="row">
          <div class="col-md-6">
          <div class="coluna">
              <h3>LOJA</h3>
              <ul>
                <li><a href="chopp/chopp-claro">Chopp Claro</a></li>
                <li><a href="chopp-escuro-2">Escuro</a></li>
                <li><a href="chopp/chopp-slow-beer">slow Beer</a></li>
                <li><a href="chopp/chopp-black">Black</a></li>
                <li><a href="chopp/chopp-puro-malte">Puro Malte</a></li>
              </ul>
            </div>
           <div class="coluna">
            <h3>INSTITUCIONAL</h3>
            <ul>
              <li><a href="index.php?route=information/capahistoria">Germânia</a></li>
              <li><a href="index.php?route=information/revendedor">Revendedor</a></li>
              <li><a href="index.php?route=information/franqueado">Franqueado</a></li>
              <li><a href="<?php echo $contact ?>">Orçamento</a></li>
              
            </ul>
          </div>

            
            <div class="coluna">
              <h3>AJUDA</h3>
              <ul>
              <li><a href="index.php?route=information/ajuda">Dúvidas</a></li>
              <li><a href="index.php?route=information/lojas">Nossas Lojas</a></li>
              </ul>
            </div>
          </div>
          <div class="col-md-6">
            <div class="coluna-info pull-right">
              <h3>Nossas Informações</h3>
              <p>Rod. Variante Vinhedo Viracopos, km 78 <br>
                CEP 13280-000 <br>
                Vinhedo - São Paulo / SP <br>
                contato@cervejariagermania.com.br<br>
                
                <strong>0800 110 420</strong>
              </p>
            </div>
          </div>
        </div>
      </div>
      <div class="linha-final">
        <div class="row text-center">
          <img src="catalog/view/theme/germania/stylesheet/images/logo-rodape.png" alt="">
        </div>
      </div>
    </footer>

   
    <script>
      



      $(document).ready(function() {
        var maioridade = localStorage.getItem("maioridade")?1:null;
        if(!maioridade){          
          $('#myModal').modal({
            backdrop: 'static',
            keyboard: false,
            show:true
          })
        }

        $("#maioridade").click(function(){
          localStorage.setItem("maioridade", 1);
        })
      });
      

      var vvTimeline;
      vvTimeline = (function() {
        var config;
        config = {
          width: '100%',
          height: '664',
          source: 'http://www.cl-ag.com/germania/historia.json?callback=onJSONP_Data',
          embed_id: 'timeline-embed',
          js: 'http://www.cl-ag.com/germania/catalog/view/javascript/timeline.js',
          css: 'http://www.cl-ag.com/new_germania/css/timeline.css',
          debug: false,
          start_zoom_adjust: 3
        };

        function vvTimeline() {window.createStoryJS(config);}

        return vvTimeline;

      })();

      var tl = new vvTimeline();
    </script>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content diferente">
          <div class="maior text-center">
            <h2>Você tem mais de <b>18 anos?</b></h2>
          </div>
          <div class="btns">
            <a href="guarana-amazonia.html" class="btn opcao nao">Não</a>
            <button id="maioridade" type="button" class="btn opcao sim" data-dismiss="modal">Sim</button>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="calculadora" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content calcu">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
            <div class="moduletable">
              <script type="text/javascript">
              jQuery(document).ready(function($)
                {
                  var tempo;var quantidade;
                  var result;

                  function verificaNumero(e)
                  {
                    if(e.which!=8&&e.which!=0&&(e.which<48||e.which>57))
                      {
                        //alert('Somente numeros');return false;
                        var btnCalc = $('#enviarCalculo');
                        btnCalc.trigger('click');
                      }
                  }
                $("#quantidade").keypress(verificaNumero);$('#enviarCalculo').on('click',function(){tempo=$('#litros option:selected').val();quantidade=$('#quantidade').val();if(tempo!=''&&quantidade!=''){if(tempo==4){result=parseFloat(quantidade)*1.5;}else if(tempo==5){result=parseFloat(quantidade)*2;}else if(tempo==6){result=parseFloat(quantidade)*2.5;}
                $('.calculadoraGeral #resultado').remove();$(this).after('<div id="resultado" class="resultado">Você deve oferecer '+result+' litro(s) de Chopp Germânia.</div>');}else{alert('Selecione o tempo de sua festa e quantas pessoas participara');}});});
              </script>
              <div class="calculadoraGeral">
                <select name="duracao" id="litros" class="duracao">
                  <option>Duração da festa</option>
                  <option value="4">Até 4 horas</option>
                  <option value="5">De 5 a 6 horas</option>
                  <option value="6">Mais que 6 horas</option>
                </select>
                <input name="quantidade" placeholder="Quatidade de pessoas" type="text" id="quantidade" class="quantidade"> 
                <input name="enviar" type="button" value="Calcular" id="enviarCalculo" class="enviarCal">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>