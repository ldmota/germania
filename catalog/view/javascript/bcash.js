// Brands regex
var brands = {
	'visa': /^4/,
	'mastercard': /^5[1-5]/,
	'diners': /^3(?:0[0-5]|[68][0-9])/,
	'elo': /^(636368|438935|504175|451416|636297|5067|4576|4011)/,
	'amex': /^3[47]/,
	'aura': /^5078/,
	'hipercard': /^(606282|3841)/
};

$('#input-card-cvv').mask('0000');
$('#input-card-number').mask('0000 0000 0000 0000');

$('#tabPayment a').on('click', function(e) {
	e.preventDefault();
	$(this).tab('show');
	
	var tab = $(this).attr('href').substr(1);
	updatePaymentMethod(tab);
});

$('.debit-brand').on('click', function(e) {
	$('.debit-brand.active').removeClass('active');
	$(this).addClass('active').prev('input[name="transferencia_brand"]').prop('checked', true);
	updatePaymentMethod('transferencia');
});

$('#formPayment #input-card-number').on('keyup', function() {
	var number = $(this).val().replace(/[^0-9]/g, ''),
		found = false;

	for(var key in brands) {
		if(new RegExp(brands[key]).test(number)) {
			found = true;
			$('#input-card-number').attr('card-brand', key);
			$('input[name="card_brand"]').val(payment_cards[key]);
			updatePaymentMethod('card');

			$('#card-installments').html('');
			for(var idx in payment_installments[key]) {
				var installment = payment_installments[key][idx];
				$('#card-installments').append('<option value="'+ installment.number +'">'+ installment.number +'x de R$ '+ installment.installmentAmount.toFixed(2).toString().replace('.', ',') + (installment.rate ? '' : ' sem Juros') +'</option>');
			}
			break;
		}
	}
	if(!found) {
		$('#input-card-number').attr('card-brand', 'unknown');
	}
});

$('#button-confirm').on('click', function() {
	var error = false;

	if($('#card').hasClass('active')) {
		$('.form-group.has-error').removeClass('has-error');
		
		if($('#input-card-holder').val() === '') {
			showError('Preencha o nome do portador do cartão', 'input-card-holder');
			error = true;
		}
		if($('#input-card-number').val() === '' && !error) {
			showError('Preencha o número do cartão', 'input-card-number');
			error = true;
		}
		if($('#input-card-cvv').val() === '' && !error) {
			showError('Preencha o código de segurança do cartão', 'input-card-cvv');
			error = true;
		}
	}

	if(!error) {
		$.ajax({
			type: 'POST',
			url: 'index.php?route=payment/bcash/confirm',
			data: {
				payment_type: $('.tab-pane.active').attr('id'),
				payment_method: $('#input-payment-method').val(),
				card_number: $('#input-card-number').val(),
				card_holder: $('#input-card-holder').val(),
				card_expiry_month: $('#card-expiry-month').val(),
				card_expiry_year: $('#card-expiry-year').val(),
				card_cvv: $('#input-card-cvv').val(),
				installment: $('#card-installments').val()
			},
			beforeSend: function() {
				$('#button-confirm').button('loading');
			},
			complete: function() {
				$('#button-confirm').button('reset');
			},
			success: function(response) {
				console.log(response);

				if(response.success !== undefined) {
					if($('#formaPagamento').val() != 'Credito') {
						window.open(response.success, '_blank');
					}

					setTimeout(function() {
						location = 'index.php?route=checkout/success';
					}, 5000);
				} else if(response.error !== undefined) {
					alert(response.error);
				}
			}
		});
	}
});

function updatePaymentMethod(tab) {
	var element = $('input[name="' + tab + '_brand"]');
	if(element.length > 1) {
		element = element.filter(':checked');
	}
	$('#input-payment-method').val(element.val());
}

function showError(error,obj) {
	$('#'+ obj).parents('.form-group').addClass('has-error');
	alert(error);
}
